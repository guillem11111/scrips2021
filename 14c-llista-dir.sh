#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
#
# llistar el directori rebut
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2

# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir..."
  exit $ERR_ARGS
fi

dir=$1

# si no és un directori plegar
if [ ! -d $dir  ]; then
	echo "ERROR: $dir no és un directori"
	echo "usage: $0 dir"
	exit $ERR_NODIR
fi

llista_dir=$(ls $dir)

for file in $llista_dir
do
	if [ -f "$dir/$file" ];
	then
		echo "$file es un regular file"
	elif [ -d "$dir/$file" ];
	then
		echo "$file es un directori"
	elif [ -h "$dir/$file" ];
	then
		echo "$file es un link"
	else
		echo "$file no es ni regular file, ni link ni dir"
	fi
done
exit 0

