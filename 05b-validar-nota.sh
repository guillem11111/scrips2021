#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
# Febrer 2022
# suspes o aprovat
# requisits:
#	validar existeix un arg
#	validar argument [0-10]
#-------------------------------

ERR_NARG=1
ERR_RANG=2

# 1) si num d'arguments no es correcte

if [ $# -ne 1 ] 
then
	echo "Error: Numero d'arguments incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARG
fi

# 2) Validat rang nota (int [0-10])

# if [ $1 -lt 0 -o $1 -gt 10 ]
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
	echo "Error: La nota ha d'estar al rang [0-10]"
	echo "Usage: $0 nota"
	exit $ERR_RANG
fi

# Xixa (aprovat/suspés)

nota=$1

if [ $nota -ge 5 ]
then
	echo "$nota: Aprovat"
else
	echo "$nota: Suspés"
fi

exit 0
