#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
# -------------------------------------

# 7) Numerar stdin i mostrar en majuscules

contador=1
while read -r line
do
	echo "$contador: $line" | tr '[:lower:]' '[:upper:]'
	((contador++))
done
exit 0



# 6) Processar stdin fins el token Fi

read -r line
while [ "$line" != "FI" ]
do
	echo "$line"
	read -r line
done
exit 0


# 5) Numera linia a linia

contador=1
while read -r line
do
	echo "$contador: $line "
	((contador++))
done
exit 0


# 4) Processar l'entrada estandard 
# linia a linia

while read -r line
do
	echo $line
done
exit 0

# 3) Mostrar arguments amb shift

while [ -n "$1" ]
do
	echo "$1, $#, $*"
	shift
done
exit 0


# 2) contador decreixent

num=$1
MIN=0

while [ $num -ge $MIN ];
do
	echo "$num"
	((num--))
done
exit 0

# 1)  Mostrar contador

MAX=10
num=1

while [ $num -le $MAX ];
do	
	echo "$num"
	((num++))
done

exit 0
