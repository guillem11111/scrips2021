#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
#
# llistar el directori rebut
# -------------------------------
ERR_ARGS=1

# si num args no es correcte plegar
if [ $# -lt 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir..."
  exit $ERR_ARGS
fi


for dir in $*
do
	# si no és un directori plegar
	if [ ! -d $dir  ]; then
		echo "ERROR: $dir no és un directori" >> /dev/stderr
	else
		llista_dir=$(ls $dir)
		echo "dir: $dir"
		for file in $llista_dir
		do
			if [ -f "$dir/$file" ];
			then
				echo -e "\t$file es un regular file"
			elif [ -d "$dir/$file" ];
			then
				echo -e "\t$file es un directori"
			elif [ -h "$dir/$file" ];
			then
				echo -e "\t$file es un link"
			else
				echo -e "\t$file no es ni regular file, ni link ni dir"
			fi
		done
	fi
done
exit 0

