#! /bin/bash
# @isx24559202 ASIX-M01
#
# Febrer 2021
# Exemple processament arguments 
# ------------------------------

echo '$*: '  $*
echo '$@: '  $@
echo '$#: '  $#
echo '$1: '  $1
echo '$2: '  $2
echo '$9: '  $9
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: '  $$
echo '$0: '  $0

nom="puig"
echo "${nom}deworld"

exit 0
