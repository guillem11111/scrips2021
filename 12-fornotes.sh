#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
# Febrer 2022
# suspes o aprovat
# requisits:
#	validar existeix un arg
#	validar argument [0-10]
#-------------------------------

ERR_ARGS=1
OK=0

if [ $# -lt 1 ] 
then
	echo "Error: Ha d'haveri almenys un argument"
	echo "Usage: $0 nota..."
	exit $ERR_ARGS
fi


for nota in $*;
do
	
	if [ $nota -lt 0 -o $nota -gt 10 ]
	then
		echo "Error: La nota $nota  ha d'estar al rang [0-10]" >> /dev/stderr
	elif [ $nota -lt 5 ]
	then
		echo "$nota: Suspès"
	elif [ $nota -lt 7 ]
	then
		echo "$nota: Aprovat"
	elif [ $nota -lt 9 ]
	then
		echo "$nota: Notable"
	else
		echo "$nota: Excel·lent"
	fi
done
exit $OK
