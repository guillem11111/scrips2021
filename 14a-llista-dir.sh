#! /bin/bash
# @guillemlopez ASIX-M01 Curs 2021-2022
#
# llistar el directori rebut
# -------------------------------
ERR_ARGS=1
ERR_NODIR=2

# si num args no es correcte plegar
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir..."
  exit $ERR_ARGS
fi

dir=$1

# si no és un directori plegar
if [ ! -d $dir  ]; then
	echo "ERROR: $dir no és un directori"
	echo "usage: $0 dir"
	exit $ERR_NODIR
fi

ls $dir
exit 0

