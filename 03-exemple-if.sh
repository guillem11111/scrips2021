#! /bin/bash 
# @isx24559020 ASIX M01-ISO 
# Febrer 2022 
# Exemple if 
# $ prog edat 
#----------------------------------

if [ $# -ne 1 ] 
then
	echo "Error: Numero d'arguments incorrecte"
	echo "Usage: $0 edad"
	exit 1
fi

if [ $1 -lt 0 ]
then
	echo "Error: Edad no pot ser inferior de zero"
	echo "Usage: edad > 0"
	exit 2
fi

edad=$1

if [ $edad -ge 18 ]
then
	echo "Edad $edad és major d'edad"
else
	echo "Edad $edad és menor d'edad"
fi

exit 0
